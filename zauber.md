--Zauber: IrrLicht--
Ein einfacher und beliebter licht Zauber 
Basiszauber: erschafft ein immaterielles licht mit der Helligkeit einer Laterne. Das licht kann entweder an einem objekt befestigt werden oder frei in der Luft schweben
Basisschwierigkeit: 1
Zauberdauer: 1 Runde
Haltedauer: 1 Stunde
Aufheben: Das Licht kann nicht ausgelöscht werden, wohl aber verdeckt werden. Der Zauberwirker kann den Zauber nach belieben aufheben, andere Zauberwirker müssen einen Gegenzauber sprechen
Spezielles: Wenn das licht frei in der Luft schwebt kann der Zauberwirker es durch Konzentration frei bewegen.


--Zauber: Trauma verschieben
Es ist viel einfacher eine Verletzung wo anders hin zu schieben als sie zu heilen...
Basisschwierigkeit: 1
Zauberdauer: 5 Runden
Basiszauber: Pro Nettoerfolg kann beim Zeil ein Trauma oder Erschöpfungskästchen zu einem anderen Attribut verschoben werden

--Zauber: Heilung--
Basisschwierigkeit: 3
Zauberdauer: 10 Runden
Basiszauber: Pro Nettoerfolg kann beim Zeil ein Traumakästchen in ein Erschöpfungskästchen umgewandelt werden oder ein Erschöpfungskästchen entfernt werden
Spezielles: Kann auf das selbe ziel erst nach einem Tag wieder angewendet werden

--Zauber: Erholung--
Basisschwierigkeit: 1
Zauberdauer: 5 Runden
Basiszauber: Entferne pr0 Nettoerfolg ein Erschöpfungskästchen vom ziel
Spezielles: Kann auf das selbe ziel erst nach einem Tag wieder angewendet werden
--Zauber: Regen rufen--
Basisschwierigkeit: 2
Zauberdauer: 10 Runden
Basiszauber: Erzeugt auf magische weise Regenwolken die in einem Bereich von ca 100m durchmesser abregnen
Haltedauer: so lange wie sich der regen natürlich halten würde

--Zauber: Erhabene Schönheit--
Ein traditioneller Zauber der Familie Liliaceae, wird gerne benutzt um sich bei festlichen Anlässen aufzubrezeln, oder um wertvolle waren noch wertvoller erscheinen zu lassen 
Basisschwierigkeit: 3
Zauberdauer: 10 Runden
Basiszauber: Das verzauberte Objekt oder Lebewesen strahlt eine ungewöhnliche Schönheit und Erhabenheit aus
Haltedauer: Bei Lebewesen ein tag, bei unbelebten Objekten ein jahr
