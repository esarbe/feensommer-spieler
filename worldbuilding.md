# Feensommer

## Einführung

Ich sehe so ein kleines Tal vor meinem geistigen Auge. Mit weiten Wiesen, dichten Wäldern und verstohlenen Lichtungen.
Ein gewaltiger Wasserfall ergiesst sich in das Tal, hinein in einen tiefen, klaren See, von welchem aus das Wasser in einem breiten, meandernden Fluß durch das Tal zieht.
Dieses Land ist frei von ... Menschen; es hat diese schon lange hinter sich zurückgelassen.
Nur verstohlen und in selten erzählten Geschichten sucht der Terror des Menschen die Kreaturen dieses zarten Landes heim.
Doch die Magie ist satt und feist in diesem Land. Sie quillt aus jeder Pore und füllt die Luft dick wie Honig.
Dies ist die Geschichte des Sommerfestes der Blumenkönigin.

Die Feenkönigin hält ihr jährliches Sommerfest. Das Sommerfest ist die Feier der Kräfte der Feenkönigin; ihrer Macht, den Sommer das ganze Jahr über dauern zu lassen. Während der Feierlichkeit wird die Magie, die von allen Feen des Reiches gesammelt wurde, zur Königin gebracht, damit sie ihre Kräfte erneuern kann. Darüber hinaus werden die unterwürfigen Kobolde, die durch die Magiegesänge der Feen in Schach gehalten werden, ihre Opfergaben präsentieren. Die Handwerker der Kobold-Clans haben ein ganzes Jahr lang an einem zarten und kostbaren Kunstwerk gearbeitet - das die Feenkönigin dann zerstören und als unzureichend im Rahmen der Zeremonie erklären wird.


## Feen und Nahrung
Feen ernähren sich nicht über Proteine, Kohlenhydrate und Fett.
Stattdessen ernähren sie sich von der Magie, welche von den Lebewesen und komplexen Netzwerken um sie herum ständig freigesetzt wird.
Hunderten von Generationen von Feen haben an der Optimierung der Praktiken und Zeremonien zur Ernte dieser Magie gearbeitet. Optimale Partnerschaften zwischen den unterschiedlichen Pflanzen und den Tieren um sie herum wurden erforscht, Kombinationen ausprobiert, Interaktionen verbessert.

Daher sind die Feen auch ständig dabei, das Ökosystem um sie herum zu massieren und zu korrigieren; dort eine Baum, welcher ein wenig zu schnell wächst, dort ein Ameisenhügel welcher überhand nimmt - die Feen sind beständig dabei, den optimalen Fluss der Magie und deren Ernte zu unterstützen. Hunderte von Feen sind alleine in einem kleinen Hain damit beschäftigt, die Magie durch das Netzwerk des Lebens um sie herum zu kanalisieren, zu konzentrieren und schlussendlich, zum Nutze des gesamten Feentums, zu sammeln.

Die Arkanisten selbst brauchen nicht viel dieser Magie für sich selbst; das meiste wird der Blumenkönigin zum Geschenk gemacht, beim alljährlichen Sommernachtsfest, damit diese die Magie zur Erneuerung des Allsommers einsetzen kann; des ewige Sommers, welcher im Reich der Sanften Götter herrscht.

Von den Elfen wird diese Magie zumeist in Form von Nektar oder Blütenblätter konsumiert; als Getränk und oder als Gelee, getrocknet, gebacken, soutiert, flambiert, kandiert - die Feenküche hat hundertausend Wege gefunden, die unterschiedlichen Formen der thaumischen Energie kulinarisch zu verarbeiten.


## Magie der Feen

Feen wirken ihre mächtigsten Zauber durch ihre Gesänge.

Es gibt auch andere Wege Magie zu wirken. Diese anderen Arten zu zaubern - durch Gesten, Gedanken, Symbole, Worte oder Miene, um nur einige zu nennen - sind unmittelbarer in ihrer Wirkung und stärker.

Feengesänge sind zart und leise. Und bergen dennoch ungeahnte Kräfte in sich. Jede noch so tödliche Wunde können sie heilen - gegeben genug anwesender Feen, natürlich, denn das Lied einer einzelnen Fee ist doch zu schwach, um viel mehr als einen Wasserfloh vor dem Herzinfarkt zu retten.
Doch schon ein kleiner Chor von Elfen, mit seinen Harmonien und Disharmonien, seinen Mustern und Variationen, seinen sich Interferenzen und Inferenzen, Suggestionen und Andeutungen kann Erstaunliches bewirken.
Nun ist es leider so das Feen sich kaum wirklich einig genug sind, um eine komplexere, kohärente Absicht in einem Gesang zu formulieren. Dazu fehlt ihnen die dafür notwendige Einstimmigkeit. So verbunden mit sich und der Natur Feen sind, so unterschiedlich sind sie in ihren Bestreben und Ansichten. Nur Nestvölker, wie Ameisen oder Bienen es sind, haben die erforderliche Unität, um auf diese verteilte Art und Weise Zauber zu wirken. Oder, sie hätten es, wenn sie Zugriff auf stukturierte und organisierte Arkana hätten, wie die Feen dies tun.

Nun ist es natürlich möglich, diese Unität zu üben und zu repetieren, sie wieder und immer wieder zu wiederholen, sie einzutrimmen, zu exerzieren und zu wiederholen holen holen holen. So lange, bis sie sitzt. So lange, bis sich gemeinsam, in den Köpfen aller partizipierenden Arkanisten, ein einzelner Gedanke, ein einzelnes Konzept formt und gemeinsam, mit den Mündern aller partizipierenden Arkanisten, ein tausendfach verschiedener und konstruierter und exakter Gesang formuliert wird. 
Das ist, wie man sich denken kann, nicht gerade ein einfaches Unterfangen. Den dafür notwenigen meditativen Geisteszustand zu erreichen ist eine Sache von jahrelangen, zähen und zermürbenden Trainieren. Und auch dann ist es schwer, mehr als einen oder zwei komplexere Zauber zu formulieren.
Einfachere Gesänge, mit weniger komplexen Zaubern, sind einfacher zu erlernen. Blutige Siege gegen die Kobolde haben die Kampfbarden der Blumenvölker errungen.

Und dann sind da natürlich noch die Wiegengesänge.
Noch bevor kleine Feen ihren Blütenknospen entschlüpfen, hören sie schon die Gesänge ihrer Eltern, welche sie hüten, beschützen, heilen und stärken. Diese einfachsten und wohl auch ältesten Gesänge der Feen sind weniger erlernt als vielmehr inhärent zu ihrem Sein. Sie sind ein Teil des Feen-seins, so weit die Feenwelt zurückdenken kann. 
Und zu diesen ältesten Gesängen der Feen gehören auch die Koboldslieder.
Niemand weiß, wann Feen diese Gesänge erlernt haben. Doch jede Familie unter der Schirmherrschaft der Blumenkönigin hat ihre eigenen Koboldslieder - manchmal mehr, manchmal weniger verschieden von denen, der anderen Famlien.
Kobolde können diese Gesänge kaum ertragen. Sie bereiten ihnen Schmerzen. Von einfachen Kopfschmerzen angefangen, über lähmende Pein bis, - so sagt man es, den es kann mehr eine Fee von sich behaupten, Zeuge gewesen zu sein - sogar zum Tod. 
In diesen zivilisierten Zeiten, in welchen kaum ein Kobold es wagt, die natürliche Ordnung in Frage zu stellen, ist dies natürlich auch nicht mehr notwenig. Nur noch selten sehen die Feen daher Bedarf, ihre Stimme gegen die Kobolde zu erheben.

## Handwerk

Feen benutzen keine Werkzeuge, um Instrumente oder andere Gegenstände herzustellen. Stattdessen lassen sie verschiedene Pflanzen in die benötigten Formen wachsen. Besonders geschickte Magier - Arkanisten genannt - können sogar mehrere verschiedene Pflanzen miteinander verwachsen und so besonders raffinierte Pflanzenmaschinen schaffen.
Diese Pflanzenmaschinen wachsen in ihre endgültige Form und müssen nicht mehr bearbeitet werden; bohren, befestigen oder schneiden ist nicht notwendig.

Die Feen benutzen aus Pflanzen gewachsene Instrumente, um Musik zu machen. So spielen sie zum Beispiel auf Grasharfen und Blütentrompeten.

Mit Hilfe eines oder zwei Tropfen kojagulierter Magie - Sonnentau -  werden diesen magisch veränderen und geformten Pflanzen zu Weil vollkommen erstaundliche Funktionen verliehen. Als Beispiel sei hier das Sprachrohr der Zelebrantin genannt; ein aus einem Löwenmäulchen gewachsene Blütenartefakt, welche ihre Stimmlautstärke verhundertfachen kann.


## Feen und Kobolde

Seit Anbeginn der Zeit lebten die Feen im Tal der Sanften Götter, im Licht der Blumenkönige und ihrem ewig währenden Sommer. Die Elfen lebten in Frieden und Harmonie mit der Natur. Bis eines Tages die Kobolde in das friedliche Tal einfielen. Mit ihren Waffen aus Stein und Knochen und Holz griffen sie die harmlosen Feen an und brachten Tod und Verderben in das bis dahin unbefleckte Tal. Von dem unerwarteten Angriff überrumpelt, mußten die Elfen zuerst schreckliche Verluste büßen. Doch die Feen sammelten sich um ihre Monarchin, fanden in ihrer Trost in ihren Worten, Mut unter ihrer Führung und Sieg mit ihrer Magie. In rascher Folge wurden die Kobolde weiter und weiter in die kargen Regionen über der Baumgrenze zurückgedrängt. Manche der Kobolde flohen in die tiefen Kavernen unter den Bergen. Doch schliesslich war der Widerstand gebrochen und die Kobolde unterwarfen sich den geeinten Familien, der Blumenkönigin an deren Spitze.


### Situation heute
Kobolde werden nach wie vor an die Rändern des Tals zurückgedrängt. Ihre Population ist groß, doch so ist die Armut und der Hunger. Viele Kobolde leben in den Höhlen unter den Bergen, oben am Rand des Waldes. 

Anders als die Feen haben die Kobolde keine scheu vor Feuer. Eine Eigenheit welche sie nicht nur abstossend menschenähnlich macht, sondern auch ungeheuer nützlich. Kunstvolle gebrannte Keramik ist so ziemlich die einzige Handelsware für Kobolde. Eine Handelware jedoch, welche ihnen zumindest einen Vorteil gegen ihre Unterdrücker gibt; sie ist hochbegehrt. Doch auch so manchem Arkanist, Pillendreher und Tränkebrauer ist mit Hilfe der Flammenkunst der Kobolde Zugang zu höchst suspektem Wissen verschafft worden.

Manche Kobolde bleiben unter sich. Die meisten jedoch sind darauf angewiesen, bei den Feen Beschäftigung zu finden. Viele verrichten grobbe und stumpfsinnige Arbeit, pflegen Gärten oder unterstützen die Feen andersweitig.

Es gibt Kobolde, die mit den Feen zusammenarbeiten und helfen, ihre eigenen Artgenossen unter Kontrolle zu halten. Die Gründe hierfür können vielfältig sein, wie zum Beispiel der Erhalt von Schutz, Nahrung und anderen Ressourcen durch die Zusammenarbeit mit den Feen. Andere wiederum haben die Geschichten und Propaganda der Feen vollständig aufgenommen und sind von der Richtigkeit ihres Handelns überzeugt.

Einige wenigen privilegierten Kobolden wird Beispiel Zugang zu magischen Kräften und Fähigkeiten gewährt.Solche Kobolde haben durch die Zusammenarbeit mit den Feen auch eine höhere Position innerhalb der Kobold-Gesellschaft erlangt. Sie werden als "Feenfreunde" bezeichnet.

Einige Familien haben angefangen, sich mit Hilfe von Kobold-Technologie gegenseitig, wortwörtlich, das Wasser abzugraben. Dies hat zwischen den grossen und kleinen Familien zu Konflikten geführt.

## Familien

Jede der Familien hatte eine besondere, kostbare magische Fähigkeit, die mit ihren spezifischen Blumen in Zusammenhang stand.

Apiaceae: Diese Familie ist nach der Familie der Doldenblütler benannt, zu der Pflanzen wie Karotten, Petersilie und Dill gehören. Ihre magische Fähigkeit besteht darin, Nahrung zu bieten, da ihre Blumen starke Nährstoffe und Heilungseigenschaften enthalten, die dazu beitragen können, das Leben aufrechtzuerhalten und Vitalität wiederherzustellen.

Asteraceae: Die Familie Asteraceae leitet ihren Namen von der Familie der Korbblütler ab, zu der Pflanzen wie Sonnenblumen, Gänseblümchen und Chrysanthemen gehören. Ihre magische Fähigkeit besteht darin, Freude und Glück zu bringen, da ihre Blumen hell, lebendig und ein Gefühl von Freude und Leichtigkeit vermitteln.

Orchidaceae: Die Familie Orchidaceae ist nach der Familie der Orchideen benannt, zu der viele exotische und seltene Orchideenarten gehören. Ihre magische Fähigkeit besteht darin, Staunen und Ehrfurcht zu inspirieren, da ihre Blumen für ihre komplizierte Schönheit, zarte Düfte und atemberaubenden Farben bekannt sind.

Liliaceae: Die Familie Liliaceae ist nach den Lilien, Tulpen und Hyazinthen benannt. Ihre magische Fähigkeit besteht darin, Reinheit und Erhabenheit zu fördern, da ihre Blumen oft von einer strahlenden Schönheit sind und ein Gefühl von Reinheit und Erhabenheit vermitteln. Sie symbolisieren auch oft die Sonne und das Licht und können dazu beitragen, die Stimmung zu heben und das Gefühl von Optimismus und Freude zu fördern.

Ranunculaceae: Die Familie Ranunculaceae leitet ihren Namen von der Familie der Hahnenfußgewächse ab, zu der Pflanzen wie Butterblumen, Anemonen und Akeleien gehören. Ihre magische Fähigkeit besteht darin, Schutz und Heilung zu bieten, da ihre Blumen starke medizinische Eigenschaften enthalten, die zur Heilung von Wunden, Abwehr von Schädlingen und zum Schutz vor Schäden verwendet werden können.

Fabaceae: Die Familie Fabaceae ist nach der Familie der Hülsenfrüchte benannt, zu der Pflanzen wie Bohnen, Erbsen und Klee gehören. Ihre magische Fähigkeit besteht darin, Wachstum und Überfluss zu fördern, da ihre Blumen für ihre Fähigkeit bekannt sind, Stickstoff im Boden zu binden und das Wachstum anderer Pflanzen zu fördern, was zu größerem Überfluss und Fruchtbarkeit im Land führt. 

Pinaceae: Diese Familie ist nach der Familie der Kieferngewächse benannt, zu der Pflanzen wie Kiefern, Fichten und Tannen gehören. Ihre magische Fähigkeit besteht darin, Schutz zu bieten, da ihre Blumen und Zapfen stark und widerstandsfähig sind und dazu beitragen können, Barrieren zu schaffen, um das Feenvolk zu schützen.

Scrophulariaceae: Die Familie Scrophulariaceae leitet ihren Namen von der Familie der Braunwurzgewächse ab, zu der Pflanzen wie Fingerhut und Löwenmäulchen gehören. Ihre magische Fähigkeit besteht darin, Heilung und Wohlbefinden zu fördern, da ihre Blumen starke medizinische Eigenschaften haben, die bei der Behandlung von Krankheiten und Verletzungen helfen können.

Solanaceae: Die Familie Solanaceae ist nach der Familie der Nachtschattengewächse benannt, zu der Pflanzen wie Tomaten, Paprika und Auberginen gehören. Ihre magische Fähigkeit besteht darin, die Träume und Visionen zu beflügeln, da ihre Blumen halluzinogene Eigenschaften haben, die dazu beitragen können, das Unterbewusstsein zu öffnen und tiefe Einsichten und Offenbarungen zu ermöglichen.

Rosaceae: Die Familie Rosaceae leitet ihren Namen von der Familie der Rosengewächse ab, zu der Pflanzen wie Rosen, Äpfel und Erdbeeren gehören. Ihre magische Fähigkeit besteht darin, Liebe und Schönheit zu fördern, da ihre Blumen für ihre Schönheit und ihren Duft bekannt sind und dazu beitragen können, das Herz zu öffnen und Beziehungen zu stärken.



##  Gesellschaftsstrukturen

### Blumenkönigin

Aus dem der Familie der Orchidaceae. Absolute und vermeindlich unumstrittene Herrscherin über das Tal der Sanften Götter.

### Der Hofstaat

Der Haushalt der Blumenkönigin

### Der Rat der Sanftmütigen

Berät die Königin in den Geschäften des Reiches.

### Pazifisten
Die Pazifisten im Tal  der Sanften Götter haben die Aufgabe, die Kobolde durch ihre Gesänge zu kontrollieren und sicherzustellen, dass sie nicht aufmüpfig werden. Sie nutzen ihre magischen Fähigkeiten, um den Frieden im Tal zu bewahren und den Kobolden keinen Anlass zur Rebellion zu geben. Dabei setzen sie ihre Kräfte nur defensiv ein und vermeiden es, Gewalt anzuwenden.

Die Tatsache, dass diese Gesänge bei den Kobolden Schmerzen verursachen können, mag zwar ein unerwünschter Nebeneffekt sein, ändert aber nichts an der Tatsache, dass das Hauptziel der Feen darin besteht, Frieden und Harmonie im Tal  der Sanften Götter zu bewahren. 

* **Schüler:** die jüngsten Mitglieder der Pazifisten und lernen noch, wie man Gesänge zur Kontrolle der Kobolde einsetzt. Sie werden von den höheren Rängen angeleitet und unterstützen bei kleineren Aufgaben.
* **Gardisten:** die Krieger unter den Pazifisten. Sie sind für den Schutz der Feen und des Tals zuständig und haben im Kampf gegen die Kobolde eine wichtige Funktion. Sie setzen ihre Gesänge ein, um die Kobolde zu schwächen und zu kontrollieren.
* **Wächter:** die Wächter und Bewahrer der Artefakte und der heiligen Orte im Tal  der Sanften Götter. Sie setzen ihre Gesänge ein, um Eindringlinge abzuwehren und das Wissen und die Macht der Feen zu schützen.
* **Meister:** die höchsten Würdenträger unter den Pazifisten. Sie sind für die Ausbildung und Führung der unteren Ränge verantwortlich und haben ein tiefes Wissen über die Magie der Feen und die Kontrolle der Kobolde.
* **Hüterin:** die Anführerin der Pazifisten und die Hüterin des Friedens im Tal  der Sanften Götter. Sie hat eine starke Verbindung zur Natur und den Geistern des Tals und setzt ihre Gesänge ein, um den Frieden und die Harmonie im Tal zu bewahren.


## Kultur

### Das Sommernachtsfest

Die Vertreter des gesamten Reiches versammeln sich zum Sommernachtsfest, um die gesamte, während des ganzen Sommerjahrs eingesammelte Magie des Reiches der Königin zum Geschenk zu machen. Dabei wird erst während eines ganzen Tages getanzt; nach einem großen Morgentanz, an welchem alle Abgesanten teilnehmen, formt sich ein grosser, verschlungener Tanzknoten, bei welchem die Teilnehmer über die Dauer von zwei Stunden immer tiefer ins Herz geführt werden, wo sie schlussendlich ihren Teil des Sonnentaus in eine zentrale Knospe tragen. Während sich die entbürdeten Tänzer aus dem Knoten entfernen, tauschen sie sich mit neuen hinzugekommen Tänzern aus. Abschluss biltet die Gabe der grosen Häuser, bei welchen die einflußreichsten Häuser des Reiches ihren Beitrag in die Knospe tragen. In der anschließende Prozession wird die mit dem Sonnentau erfüllte Knospe, von Tanz, Gesang und Musik begleitet, in den Palastbaum der Königin getragen, wo ihr diese zum Geschenk gemacht wird. 
